import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'

export default async (fastify: FastifyInstance) => {

  fastify.post('/', (_request: FastifyRequest, reply: FastifyReply) => {
    reply.send({ message: 'Message from POST method' })
  })

  fastify.post('/params', (request: FastifyRequest, reply: FastifyReply) => {
    const body: any = request.body
    const username = body.username
    const password = body.password

    console.log({ username })
    console.log({ password })

    reply.send(body)
  })

  fastify.get('/', (_request: FastifyRequest, reply: FastifyReply) => {
    reply.send({ message: 'Message from GET method' })
  })

  fastify.get('/:firstName/:lastName', (request: FastifyRequest, reply: FastifyReply) => {
    const params: any = request.params
    const firstName = params.firstName
    const lastName = params.lastName

    reply.send({ firstName, lastName })
  })

  fastify.get('/query', (request: FastifyRequest, reply: FastifyReply) => {
    const query = request.query
    reply.send(query)
  })

  fastify.put('/', (_request: FastifyRequest, reply: FastifyReply) => {
    reply.send({ message: 'Message from PUT method' })
  })

  fastify.put('/:userId', (request: FastifyRequest, reply: FastifyReply) => {
    const body: any = request.body
    const params: any = request.params

    const username = body.username
    const password = body.password
    const userId = params.userId

    reply.send({ username, password, userId })
  })

  fastify.delete('/', (_request: FastifyRequest, reply: FastifyReply) => {
    reply.send({ message: 'Message from DELETE method' })
  })

  fastify.delete('/:userId', (request: FastifyRequest, reply: FastifyReply) => {
    const params: any = request.params

    const userId = params.userId

    reply.send({ message: `User id ${userId} has been deleted!` })
  })

  /*
eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJtZXNzYWdlIjoiRmFzdGlmeSB2ZXJ5IGZhc3QhIiwic2NvcGUiOiJzdGF0dXMgdXNlcjpyZWFkIHVzZXI6d3JpdGUiLCJyb2xlIjpbImFkbWluIiwidXNlciJdLCJpYXQiOjE2NjExMzg0NDV9.7UH4nFXz_3PESYSpcfWYIyKqw4tiePg4JwaGYy5VGGQ
  */
  fastify.get(
    '/guard',
    {
      preValidation: [fastify.authenticate],
      preHandler: [fastify.guard.scope(['user:read', 'user:write'])]
    },
    (req, reply) => {
      reply.send(req.user)
    }
  )

  fastify.get(
    '/guard-role',
    {
      preValidation: [fastify.authenticate],
      preHandler: [fastify.guard.role(['admin'])]
    },
    (req, reply) => {
      reply.send(req.user)
    }
  )
}
