import fp from 'fastify-plugin'
import { Server } from 'socket.io';

module.exports = fp(async function (fastify: any, opts: any, next: any) {

  const io = new Server(fastify.server, opts);

  fastify.decorate('io', io)
  next()

}, { fastify: '4.x', name: 'fastify/io' })