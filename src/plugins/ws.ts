import fp from 'fastify-plugin'
const WebSocketServer = require('ws').Server

module.exports = fp(async function (fastify: any, opts: any, next: any) {

  const wsOpts: any = {
    server: fastify.server
  }

  if (opts.path !== undefined) {
    wsOpts.path = opts.path
  }

  const wss = new WebSocketServer(wsOpts)

  fastify.decorate('ws', wss)

  fastify.addHook('onClose', (fastify: any, done: any) => fastify.ws.close(done))

  next()

}, { fastify: '4.x', name: 'fastify/ws' })