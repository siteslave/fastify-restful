import Knex from "knex";
import FastifyGuard from 'fastify-guard'

import * as jsonwebtoken from 'jsonwebtoken';
import { AxiosInstance } from 'axios';
import PinoPretty from "pino-pretty";

declare module 'fastify' {
  interface FastifyInstance {
    db: Knex
    db2: Knex
    jwt: jsonwebtoken
    authenticate: any
    ws: any
    io: any
    axios: AxiosInstance | any
    qrcode: any
    guard: FastifyGuard
  }

  interface FastifyRequest {
    jwtVerify: any
    user: any
    file: any
    files: any[]
    log: PinoPretty
  }

}

